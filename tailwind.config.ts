export const COLORS = {
   baseGray2: '#191919',
   baseGray3: '#313131',
   baseGray4: '#A4A4A4',
   baseGray5: '#515151',
   baseBlack: '#020202',
   greenNeon: '#B3FC4F',
   blackGreen: '#173102',
} as const;

/** @type {import('tailwindcss').Config} */
module.exports = {
   content: [
      './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
      './src/components/**/*.{js,ts,jsx,tsx,mdx}',
      './src/app/**/*.{js,ts,jsx,tsx,mdx}',
   ],
   theme: {
      extend: {
         colors: COLORS,
      },
   },
   plugins: [],
   COLORS,
};
