import PUBLIC_RUNTIME_CONFIG from './src/app/shared/utils/publicRuntimeConfig.mjs';

/** @type {import('next').NextConfig} */
const nextConfig = {
    reactStrictMode: true,
    publicRuntimeConfig: PUBLIC_RUNTIME_CONFIG,
};

export default nextConfig;
