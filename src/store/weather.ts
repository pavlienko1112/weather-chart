import {makeAutoObservable} from 'mobx';
import {Coordinates} from "@/app/shared/types/coordinates";
import axios from "axios";
import {CITIES_COORDINATES} from "@/app/shared/constants/cities";
import {CityWeather, CityWeatherRecord, NamedCityWeather} from "@/app/shared/types/city";
import {ENV_VARS} from "@/app/shared/utils/envVariables";

class Weather {
   selectedCityWeather: NamedCityWeather | null = null;
   allCountriesWeather: CityWeatherRecord = {} as CityWeatherRecord;

   constructor() {
      makeAutoObservable(this);
      this.fetchAllCitiesWeather().then()
   }

   async fetchAllCitiesWeather() {
      const citiesWeather = await Promise.all(Object.entries(CITIES_COORDINATES).map(async ([city, coordinates]) =>
          ({city, weather: await this.fetchCityWeather(coordinates)})))
      const mappedCitiesWeather = citiesWeather.reduce((prev, {city, weather}) => ({...prev, [city]: weather}), {} as CityWeatherRecord)

      this.allCountriesWeather = mappedCitiesWeather;
      return mappedCitiesWeather;
   }

   async fetchCityWeather({latitude, longitude, timezone = Intl.DateTimeFormat().resolvedOptions().timeZone}: Coordinates & { timezone?: string }) {
      try {
         const response = await axios.get<CityWeather>(`${ENV_VARS.API_URL}/forecast?timezone=${timezone}&latitude=${latitude}&longitude=${longitude}&daily=apparent_temperature_max,apparent_temperature_min,winddirection_10m_dominant`);
         return response.data;
      } catch (e) {
         console.warn(e);
      }
   }

   setSelectedCountry(country: typeof this.selectedCityWeather) {
      this.selectedCityWeather = country;
   }
}

export default new Weather();
