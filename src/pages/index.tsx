import CitiesTable from "@/app/features/cities/components/CitiesTable";
import TemperatureChart from "@/app/features/charts/components/TemperatureChart";

function HomePage() {

   return (
       <main className='grid gap-8'>
          <CitiesTable/>
          <TemperatureChart/>
       </main>
   );
}

export default HomePage;
