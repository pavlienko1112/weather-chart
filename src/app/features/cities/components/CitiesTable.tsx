import {ChangeEventHandler, FC, useState} from 'react'
import Table from "@/app/shared/components/ui/Table/Table";
import {TABLE_HEADERS} from "@/app/features/cities/constants/CitiesTable";
import {observer} from "mobx-react-lite";
import Weather from "@/store/weather";
import {findMean} from "@/app/shared/utils/math";
import DropdownInput from "@/app/shared/components/ui/DropdownInput/DropdownInput";
import {CITIES_COORDINATES, CITY_WEATHER_MAX_TEMPERATURE, CITY_WEATHER_MIN_TEMPERATURE} from "@/app/shared/constants/cities";
import {Option} from "@/app/shared/types/option";
import {validateTemperature} from "@/app/features/cities/utils/filters";
import {SplitItemIntoRowHandler} from "@/app/shared/components/ui/Table/types";
import {OptionClickHandler} from "@/app/shared/components/ui/DropdownInput/types";

type Props = {};

const CitiesTable: FC<Props> = ({}) => {
   const {allCountriesWeather} = Weather;
   const [selectedCities, setSelectedCities] = useState<Option[]>([]);
   const [minTemperature, setMinTemperature] = useState<number | ''>('');
   const [maxTemperature, setMaxTemperature] = useState<number | ''>('');

   const handleCityOptionClick: OptionClickHandler<Option> = ({text, value}) => {
      const newSelectedCities = [...selectedCities];
      const clickedOptionIndex = selectedCities.findIndex((city) => city.value === value);

      if (clickedOptionIndex !== -1) newSelectedCities.splice(clickedOptionIndex, 1);
      else newSelectedCities.push({value, text});

      setSelectedCities(newSelectedCities);
   };

   const handleTemperatureChange: ChangeEventHandler<HTMLInputElement> = (e) => {
      const {name, value} = e.target;
      const minValue = name === 'maxTemperature' ? minTemperature || CITY_WEATHER_MIN_TEMPERATURE : CITY_WEATHER_MIN_TEMPERATURE;
      const maxValue = name === 'minTemperature' ? maxTemperature || CITY_WEATHER_MAX_TEMPERATURE : CITY_WEATHER_MAX_TEMPERATURE;
      const inputValue = +value;
      const validatedValue = validateTemperature({initialValue: inputValue, minValue, maxValue});

      if (name === 'maxTemperature') {
         setMaxTemperature(validatedValue);
      } else {
         setMinTemperature(validatedValue);
      }
   };

   const splitCityInfoIntoRowCells: SplitItemIntoRowHandler<(typeof tableBody)[number]> = (cityWeather) => {
      const {city, daily: {apparent_temperature_max, apparent_temperature_min, winddirection_10m_dominant}} = cityWeather;

      return [
         <button onClick={handleCityClick(cityWeather)} className='text-start underline underline-offset-2 block text-[inherit] capitalize'>{city}</button>,
         findMean(apparent_temperature_max),
         findMean(apparent_temperature_min),
         findMean(winddirection_10m_dominant),
      ];
   }

   const handleCityClick = (city: (typeof tableBody)[number]) => {
      return () => {
         Weather.setSelectedCountry(city);
      }
   }

   const citiesOptions: Option[] = Object.keys(CITIES_COORDINATES).map((city) => ({value: city, text: city}));

   const isTemperatureInRange = ({maxTemps, minTemps}: { minTemps: number[], maxTemps: number[] }) => {
      const isTemperatureHigherMinima = (minTemperature ? minTemperature >= findMean(minTemps) : true);
      const isTemperatureLowerMaxima = (maxTemperature ? maxTemperature >= findMean(maxTemps) : true);
      return isTemperatureHigherMinima && isTemperatureLowerMaxima;
   };

   const isSelectedCity = (city: string) => {
      return selectedCities.length === 0 ? true : selectedCities.some(({value}) => value === city);
   };

   const tableBody = Object.entries(allCountriesWeather).map(([city, weather]) => ({city, ...weather}));
   const filteredTableBody = tableBody.filter(({daily: {apparent_temperature_max: maxTemps, apparent_temperature_min: minTemps}, city}) =>
       isTemperatureInRange({minTemps, maxTemps}) && isSelectedCity(city));

   return (
       <div>
          <div className="flex gap-2 mb-[25px]">
             <DropdownInput placeholder="Country" selectedOptions={selectedCities} onOptionClick={handleCityOptionClick} options={citiesOptions}/>
             <DropdownInput step={1} name='maxTemperature' onInputChange={handleTemperatureChange} type='number' max={CITY_WEATHER_MAX_TEMPERATURE}
                            min={minTemperature} value={maxTemperature} placeholder="Max"/>
             <DropdownInput step={1} name='minTemperature' onInputChange={handleTemperatureChange} type='number' max={maxTemperature}
                            min={CITY_WEATHER_MIN_TEMPERATURE} value={minTemperature} placeholder="Min"/>
          </div>
          <Table tableHeadersData={TABLE_HEADERS} tableBodyData={filteredTableBody} splitItemIntoRowDataArray={splitCityInfoIntoRowCells}/>
       </div>
   );
};

export default observer(CitiesTable);
