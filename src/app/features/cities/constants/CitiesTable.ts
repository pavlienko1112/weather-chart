import {TableHeaderCell} from "@/app/shared/components/ui/Table/types";

export const TABLE_HEADERS: TableHeaderCell[] = [
   {text: 'City', style: {textAlign: 'start'}},
   {text: 'Temperature max'},
   {text: 'Temperature min'},
   {text: 'Wind direction'},
];
