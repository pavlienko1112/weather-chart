export const validateTemperature = ({initialValue, minValue, maxValue}: { initialValue: number, minValue: number, maxValue: number }) => {
   let validatedNumber = initialValue;
   if (validatedNumber >= maxValue) validatedNumber = maxValue - 1;
   if (validatedNumber <= minValue) validatedNumber = minValue + 1;
   return validatedNumber === 0 ? '' : parseInt(String(validatedNumber));
};
