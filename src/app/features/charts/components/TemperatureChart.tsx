import {FC} from 'react'
import {Bar, BarChart, CartesianGrid, Legend, ResponsiveContainer, XAxis, YAxis} from "recharts";
import dynamic from "next/dynamic";
import Weather from "@/store/weather";
import {DAYS_OF_WEEK} from "@/app/shared/constants/week";
import {findMean} from "@/app/shared/utils/math";
import {Formatter} from "recharts/types/component/DefaultLegendContent";
import TemperatureChartLegend from "@/app/features/charts/components/TemperatureChartLegend";
import {observer} from "mobx-react-lite";
import {COLORS} from "../../../../../tailwind.config";

type ChartProps = {};

const TemperatureChart: FC<ChartProps> = ({}) => {
   const {allCountriesWeather, selectedCityWeather} = Weather;
   const cityWeather = selectedCityWeather ? selectedCityWeather : Object.values(allCountriesWeather)[0];
   if (Object.values(cityWeather).length === 0) return null;

   const {daily: {apparent_temperature_max, apparent_temperature_min}} = cityWeather;

   const chartData = DAYS_OF_WEEK.map((day, i) => ({
      name: day.slice(0, 3),
      temperature: findMean([apparent_temperature_max[i], apparent_temperature_min[i]])
   }));

   const formatLegendLabel: Formatter = (value) => {
      return value[0].toUpperCase() + value.slice(1);
   }

   return (
       <ResponsiveContainer width='100%' height={300}>
          <BarChart data={chartData}>
             <Legend content={TemperatureChartLegend} wrapperStyle={{marginTop: '-22px'}}
                     verticalAlign="top" align='right' iconSize={10} iconType='square' formatter={formatLegendLabel}/>
             <CartesianGrid vertical={false} className='stroke-baseGray3' strokeDasharray="10 7"/>
             <XAxis axisLine={false} tick={{fill: 'var(--text-color)'}} tickLine={false} dataKey="name"/>
             <YAxis axisLine={false} tick={{fill: COLORS.baseGray4, fontWeight: 500}} tickLine={false} dataKey='temperature'/>
             <Bar maxBarSize={30} radius={[8, 8, 0, 0]} dataKey="temperature" fill="url(#colorGradient)"/>
             <defs>
                <linearGradient id="colorGradient" x1="0" y1="0" x2="0" y2="1">
                   <stop offset="0%" stopColor={COLORS.greenNeon}/>
                   <stop offset="100%" stopColor={COLORS.blackGreen}/>
                </linearGradient>
             </defs>
          </BarChart>
       </ResponsiveContainer>
   );
};

export default dynamic(() => Promise.resolve(observer(TemperatureChart)), {
   ssr: false
});

