import {Props} from "recharts/types/component/DefaultLegendContent";
import Weather from "@/store/weather";

const TemperatureChartLegend = ({payload, iconSize, formatter}: Props) => {
   const {selectedCityWeather, allCountriesWeather} = Weather;
   const cityName = selectedCityWeather?.city || Object.keys(allCountriesWeather)[0];

   return (
       <div className='flex justify-between'>
          <header className='font-bold text-[16px] capitalize'>
             Analytics (Mean of both min and max temps) {cityName ? `- ${cityName}` : ''}
          </header>
          <ul className='grid gap-1.5'>
             {payload?.map((legendItem, index) => {
                const legendLabel = formatter ? formatter(legendItem.value, legendItem, index) : legendItem.value;

                return (
                    <li className='flex gap-2 items-center' key={`${index}`}>
                       <svg width={iconSize} height={iconSize}>
                          <rect width={iconSize} height={iconSize} fill={`${legendItem.color}`}/>
                       </svg>
                       <span>{legendLabel}</span>
                    </li>
                )
             })}
          </ul>
       </div>
   );
}

export default TemperatureChartLegend;
