import {ComponentProps, ReactNode} from "react";

export type TableHeaderCell = {
   text: string
} & ComponentProps<'th'>;

export type SplitItemIntoRowHandler<T> = (item: T) => ReactNode[];
