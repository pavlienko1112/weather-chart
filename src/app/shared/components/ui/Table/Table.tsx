import {SplitItemIntoRowHandler, TableHeaderCell} from "@/app/shared/components/ui/Table/types";

type Props<T extends {}> = {
   tableHeadersData: TableHeaderCell[],
   tableBodyData: T[],
   splitItemIntoRowDataArray: SplitItemIntoRowHandler<T>;
}

const Table = <T extends {}>({tableHeadersData, tableBodyData, splitItemIntoRowDataArray}: Props<T>) => {
   const CELL_WIDTH = `calc(100% / ${tableHeadersData.length})`;
   const isTableBodyEmpty = tableBodyData.length === 0;

   return (
       <div className='rounded-xl overflow-hidden border border-1 border-baseGray3'>
          <table className='w-full'>
             <thead className='bg-baseBlack'>
             <tr>
                {tableHeadersData.map(({text, style = {}, className, ...props}) => (
                    <th className={`text-end py-[9px] border-r-[1px] last:border-r-[0] border-baseGray2 px-4 ${className}`} style={{width: CELL_WIDTH, ...style}} key={text} {...props}>
                       {text}
                    </th>
                ))}
             </tr>
             </thead>

             <tbody>
             {!isTableBodyEmpty && tableBodyData.map((item, idx) => (
                 <tr className='odd:bg-baseGray2 even:bg-baseGray3' key={idx}>
                    {splitItemIntoRowDataArray(item).map((cellElement, i) => (
                            <td className='text-end py-[9px] px-4' style={{width: CELL_WIDTH}} key={i}>
                               <div>{cellElement}</div>
                            </td>
                        )
                    )}
                 </tr>))}
             </tbody>
          </table>
       </div>
   );
};

export default Table;
