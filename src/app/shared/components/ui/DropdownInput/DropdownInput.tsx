import {ChangeEventHandler, ComponentProps, MouseEventHandler, useRef, useState} from "react";
import Image from "next/image";
import {Option} from "@/app/shared/types/option";
import {OptionClickHandler} from "@/app/shared/components/ui/DropdownInput/types";

type Props<T extends Option> = {
   selectedOptions?: T[],
   options?: T[],
   onOptionClick?: OptionClickHandler<T>,
   onInputChange?: ChangeEventHandler<HTMLInputElement>,
} & ComponentProps<"input">;

const DropdownInput = <T extends Option>({options, onOptionClick, selectedOptions, onInputChange, className, ...props}: Props<T>) => {
   const [isDropdownVisible, setIsDropdownVisible] = useState(false);
   const [inputValue, setInputValue] = useState("");
   const inputRef = useRef<HTMLInputElement>(null);

   const handleInputChange: ChangeEventHandler<HTMLInputElement> = (e) => {
      onInputChange && onInputChange(e);
      setInputValue(e.target.value);
   };

   const handleDropdownMouseEnter: MouseEventHandler = () => {
      inputRef.current!.focus();

      if (!options) return;
      setIsDropdownVisible(true);
   };

   const handleDropdownMouseLeave: MouseEventHandler = () => {
      setIsDropdownVisible(false);
   };

   const handleOptionClick = (option: T) => {
      return () => {
         onOptionClick && onOptionClick(option);
      };
   };

   const filteredOption = options?.filter(option => option.text.toLowerCase().includes(inputValue.toLowerCase()));

   return (
       <div onMouseLeave={handleDropdownMouseLeave} className="relative bg-baseGray3 w-40 border-baseGray5 border-[1px] rounded-xl border-1">
          <div className="flex justify-between items-center py-[11px] px-4" onMouseEnter={handleDropdownMouseEnter}>
             <input ref={inputRef} value={inputValue} onChange={handleInputChange}
                    className={`w-full font-medium leading-[18px] bg-transparent text-baseGray4 ${className}`} {...props}/>
             <Image width={16} height={16} src="/icons/chevron-down.svg" alt="chevron"/>
          </div>

          {filteredOption && (
              <div className={`${isDropdownVisible ? "opacity-100 visible" : "opacity-0 invisible"} max-h-[400px] z-10 overflow-y-auto absolute w-full
               rounded-[inherit] bg-baseGray4 grid bottom-0 left-0 translate-y-full`}>
                 {filteredOption.map((option) => {
                    const isCurrentOptionSelected = selectedOptions?.some(({value}) => value === option.value);

                    return (
                        <button onClick={handleOptionClick(option)} key={option.value}
                                className={`text-baseBlack py-1 ${isCurrentOptionSelected && "bg-red-500"}`}>
                           {option.text}
                        </button>
                    );
                 })}
              </div>)
          }
       </div>
   );
};

export default DropdownInput;
