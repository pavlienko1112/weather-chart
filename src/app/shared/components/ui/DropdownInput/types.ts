import {Option} from "@/app/shared/types/option";

export type OptionClickHandler<T extends Option> = (option: T) => void;
