import {CITIES_COORDINATES} from "@/app/shared/constants/cities";

export type CityWeather = {
   latitude: number;
   longitude: number;
   utc_offset_seconds: number;
   daily_units: {
      time: string;
      apparent_temperature_max: string;
      apparent_temperature_min: string;
      winddirection_10m_dominant: string;
   };
   daily: {
      time: string[];
      apparent_temperature_max: number[];
      apparent_temperature_min: number[];
      winddirection_10m_dominant: number[];
   };
};
export type City = keyof typeof CITIES_COORDINATES;
export type CityWeatherRecord = Record<City, CityWeather>
export type NamedCityWeather = CityWeather & { city: string }
