export function findMean(arr: number[], precision: number = 2) {
    return +(arr.reduce((prev, next) => prev + next, 0) / arr.length).toFixed(precision);
}
