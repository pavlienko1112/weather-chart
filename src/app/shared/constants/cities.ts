import {Coordinates} from "@/app/shared/types/coordinates";

export const CITIES_COORDINATES = {
   kyiv: {latitude: 50.4547, longitude: 30.5238},
   dnipro: {latitude: 48.4647, longitude: 35.0462},
   odesa: {latitude: 46.4825, longitude: 30.7233},
   london: {latitude: 51.5074, longitude: -0.1278},
   helsinki: {latitude: 60.1699, longitude: 24.9384},
   venice: {latitude: 45.4408, longitude: 12.3155},
   luxembourg: {latitude: 49.6116, longitude: 6.1319},
   amsterdam: {latitude: 52.3702, longitude: 4.8952},
} satisfies Record<string, Coordinates>;

export const CITY_WEATHER_MAX_TEMPERATURE = 80;
export const CITY_WEATHER_MIN_TEMPERATURE = -80;
